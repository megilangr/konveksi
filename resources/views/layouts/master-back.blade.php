<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Yoshida | Baby & Sleepwear</title>
	<link rel="icon" href="{{ asset('frontend2') }}/img/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/themify-icons.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/metisMenu.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/owl.carousel.min.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/slicknav.min.css">
	<!-- amchart css -->
	<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
	<!-- others css -->
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/typography.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/default-css.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/styles.css">
	<link rel="stylesheet" href="{{ asset('backend/assets2') }}/css/responsive.css">
	<!-- modernizr css -->
	<script src="{{ asset('backend/assets2') }}/js/vendor/modernizr-2.8.3.min.js"></script>

	{{-- Other --}}

	<link rel="stylesheet" href="{{ asset('backend') }}/datatables/datatables.min.css" type="text/css">
	<link rel="stylesheet" href="{{ asset('backend') }}/lightbox2/css/lightbox.min.css" type="text/css">
	
	<link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
	<script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>

	<style>
		div.dataTables_wrapper div.dataTables_length select {
			width: 60px !important;
		}

		.act-button {
			margin-top: -20px !important;
		}

		select.form-control:not([size]):not([multiple]) {
			height: 44px !important;
		}
	</style>

	@yield('css')
</head>

<body>
	<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
	<!-- preloader area start -->
	<div id="preloader">
		<div class="loader"></div>
	</div>
	<!-- preloader area end -->
	<!-- page container area start -->
	<div class="page-container sbar_collapsed">
		<!-- sidebar menu area start -->
		<div class="sidebar-menu">
			<div class="sidebar-header">
				<div class="logo" style="background-color: #ffffff;">
					<a href="index.html"><img src="{{ asset('frontend2') }}/logo.png" alt="logo"></a>
				</div>
			</div>
			<div class="main-menu">
				<div class="menu-inner">
					<nav>
						<ul class="metismenu" id="menu">
							<li>
								<a href="{{ url('/home') }}" aria-expanded="true"><i class="ti-home"></i><span>Halaman Utama</span></a>
							</li>
							<li>
								<a href="javascript:void(0)" aria-expanded="true"><i class="ti-pencil-alt"></i><span>Form Master</span></a>
								<ul class="collapse">
									<li><a href="{{ route('category.index') }}">Kategori</a></li>
									<li><a href="{{ route('product.index') }}">Produk</a></li>
								</ul>
							</li>
							<li>
								<a href="javascript:void(0)" aria-expanded="true"><i class="ti-shopping-cart"></i><span>Transaksi</span></a>
								<ul class="collapse">
									<li><a href="{{ url('/order') }}">Data Pemesanan</a></li>
									<li><a href="{{ url('/confirmAdmin') }}">Konfirmasi Pemesanan</a></li>
									<li><a href="{{ url('/paymentConfirm') }}">Konfirmasi Pembayaran</a></li>
								</ul>
							</li>
							@if (auth()->user()->role == 'admin')
							<li>
								<a href="{{ url('/user') }}" aria-expanded="true"><i class="ti-user"></i><span>Manajemen User</span></a>
							</li>
							@endif
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<!-- sidebar menu area end -->
		<!-- main content area start -->
		<div class="main-content">
			<!-- header area start -->
			<div class="header-area">
				<div class="row align-items-center">
					<!-- nav and search button -->
					<div class="col-md-6 col-sm-8 clearfix">
						<div class="nav-btn pull-left">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<div class="search-box pull-left">
							<form action="#">
								<input type="text" name="search" placeholder="Search..." required>
								<i class="ti-search"></i>
							</form>
						</div>
					</div>
					<!-- profile info & task notification -->
					<div class="col-md-6 col-sm-4 clearfix">
						<ul class="notification-area pull-right">
							<li id="full-view"><i class="ti-fullscreen"></i></li>
							<li id="full-view-exit"><i class="ti-zoom-out"></i></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- header area end -->
			<!-- page title area start -->
			<div class="page-title-area">
				<div class="row align-items-center">
					<div class="col-sm-6">
						<div class="breadcrumbs-area clearfix">
							<h4 class="page-title pull-left">Dashboard</h4>
							<ul class="breadcrumbs pull-left">
								<li><a href="{{ url('/home') }}">Home</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 clearfix">
						<div class="user-profile pull-right">
							<img class="avatar user-thumb" src="{{ asset('backend/assets2') }}/images/author/avatar.png" alt="avatar">
							<h4 class="user-name dropdown-toggle" data-toggle="dropdown">{{ auth()->user()->name }} <i class="fa fa-angle-down"></i>
							</h4>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="{{ url('/change-password') }}">Ubah Password</a>
								<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a>
								
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- page title area end -->
			<div class="main-content-inner">
				<div class="row">
					<div class="col-12 mt-3">
						@yield('content')
					</div>
				</div>
			</div>
		</div>
		<!-- main content area end -->
		<!-- footer area start-->
		<footer>
			<div class="footer-area">
				<p>© Copyright 2020. All right reserved.</p>
			</div>
		</footer>
		<!-- footer area end-->
	</div>
	<!-- page container area end -->
	
	<!-- jquery latest version -->
	<script src="{{ asset('backend/assets2') }}/js/vendor/jquery-2.2.4.min.js"></script>
	<!-- bootstrap 4 js -->
	<script src="{{ asset('backend/assets2') }}/js/popper.min.js"></script>
	<script src="{{ asset('backend/assets2') }}/js/bootstrap.min.js"></script>
	<script src="{{ asset('backend/assets2') }}/js/owl.carousel.min.js"></script>
	<script src="{{ asset('backend/assets2') }}/js/metisMenu.min.js"></script>
	<script src="{{ asset('backend/assets2') }}/js/jquery.slimscroll.min.js"></script>
	<script src="{{ asset('backend/assets2') }}/js/jquery.slicknav.min.js"></script>

	<!-- others plugins -->
	<script src="{{ asset('backend/assets2') }}/js/plugins.js"></script>
	<script src="{{ asset('backend/assets2') }}/js/scripts.js"></script>

	
	{{-- Other --}}
	<script src="{{ asset('backend') }}/datatables/datatables.min.js"></script>
	<script src="{{ asset('backend') }}/lightbox2/js/lightbox.min.js"></script>

	<script>
		$(document).ready(function () {
			var flash = "{{ Session::has('status') }}";
			if (flash) {
				var status = "{{ Session::get('status') }}";
				swal('success', status, 'success');
			}
		});
	</script>

	@yield('script')
</body>

</html>