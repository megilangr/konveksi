<!doctype html>
<html lang="zxx">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Yoshida | Baby & Sleepwear</title>
	<link rel="icon" href="{{ asset('frontend2') }}/img/favicon.png">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/bootstrap.min.css">
	<!-- animate CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/animate.css">
	<!-- owl carousel CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/owl.carousel.min.css">
	<!-- font awesome CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/all.css">
	<!-- flaticon CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/flaticon.css">
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/themify-icons.css">
	<!-- font awesome CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/magnific-popup.css">
	<!-- swiper CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/slick.css">
	<!-- style CSS -->
	<link rel="stylesheet" href="{{ asset('frontend2') }}/css/style.css">

	<link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
	<script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>

	<link rel="stylesheet" href="{{ asset('backend') }}/datatables/datatables.min.css" type="text/css">

	@yield('css')
</head>

<body>
	<!--::header part start::-->
	<header class="main_menu home_menu">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-lg-12">
					<nav class="navbar navbar-expand-lg navbar-light">
						<a class="navbar-brand" href="{{ url('/') }}"> <img src="{{ asset('frontend2') }}/logo.png" alt="logo"
								style="height: 80px !important;">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="menu_icon"><i class="fas fa-bars"></i></span>
						</button>

						<div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
							<ul class="navbar-nav">
								<li class="nav-item">
									<a class="nav-link" href="{{ url('/') }}">Home</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ url('/list-product') }}">Produk</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ url('/about') }}">Tentang Kami</a>
								</li>
							</ul>
						</div>
						<div class="hearer_icon d-flex align-items-center">
							@if (auth()->check())
							<div class="dropdown">
								<a class="btn btn-sm btn-default btn-icon-only" href="#" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
									<i class="ti-user"></i>
								</a> Hi, {{ Auth::user()->name }}
								<div class="dropdown-menu">
									<a class="dropdown-item" href="{{ url('/change-password') }}">Ubah Password</a>
									<a class="dropdown-item" href="{{ url('/invoice/list') }}">List Pemesanan</a>
									<a class="dropdown-item" href="#"
										onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
								</div>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
							@else
							<a id="search_1" href="{{ route('login') }}"><i class="ti-user"></i></a>
							@endif
							<a href="{{ url('/shopping-cart') }}">
								<i class="flaticon-shopping-cart-black-shape"></i>
							</a>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<!-- Header part end-->

	<!-- banner part start-->
	<section class="banner_part" {{ Request::is(['/']) ? '':'hidden' }}>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-5">
					<div class="banner_text">
						<div class="banner_text_iner">
							<h1>Konveksi Berkualitas</h1>
							<p>Silahkan pesan segala jenis pakaian apapun di Toko kami !</p>
							<a href="product_list.html" class="btn_1">Buat Pemesanan</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="banner_img">
			<img src="{{ asset('frontend2') }}/img/banner.png" alt="#" class="img-fluid">
			<img src="{{ asset('frontend2') }}/img/banner_pattern.png " alt="#" class="pattern_img img-fluid">
		</div>
	</section>
	<!-- banner part start-->

	<section class="trending_items pt-0" {{ Request::is(['/']) ? '':'hidden' }}>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section_tittle text-center">
						<h2 class="mt-4 mb-4 bg-white pt-5 pb-5">Produk Terbaru</h2>
					</div>
				</div>
			</div>
			<div class="row">
				@forelse (\App\Product::orderBy('created_at','desc')->where('status','publish')->paginate(3); as $item)
				<div class="col-lg-4 col-md-4 col-sm-6 {{ str_replace(' ','', $item->category->name) }}">
					<div class="single_product_item text-center">
						<a href="{{ url('product/detail', $item->id) }}">
							<img src="{{ url($item->image) }}" alt="#" class="img-fluid" style="height: 200px !important;">
						</a>
						<h3> <a href="{{ url('product/detail', $item->id) }}">{{ $item->name }}</a> </h3>
						<p>Rp. {{ number_format($item->price, 0, ',', '.') }}</p>
					</div>
				</div>
				@empty
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="single_product_item text-center">
						<h3> Belum Ada Data Produk</h3>
					</div>
				</div>
				@endforelse
			</div>
		</div>
	</section>


	<section class="product_list section_padding" {{ Request::is(['list-product', 'shopping-cart', 'about', 'invoice*']) ? '':'hidden' }} style="padding: 50px 0px;">
		<div class="container">
			<div class="row">
				@yield('content')
			</div>
		</div>
	</section>

	<div class="product_image_area" {{ Request::is('product*') ? '':'hidden' }} style="margin-top: 50px !important;">
		<div class="container">
			<div class="row">
				@yield('content-detail')
			</div>
		</div>
	</div>

	<section class="login_part section_padding" {{ Request::is(['login', 'register', 'change-password']) ? '':'hidden' }}
		style="margin-top: 10px !important; padding-top: 10px !important;">
		<div class="container">
			<div class="row align-items-center">
				@yield('content-login')
			</div>
		</div>
	</section>

	<!--::footer_part start::-->
	<footer class="footer_part">
		<!--<div class="footer_iner">
			<div class="container">
				<div class="row justify-content-between align-items-center">
					<div class="col-lg-8">
						<div class="footer_menu">
							<div class="footer_logo">
								<a href="{{ url('/') }}"><img src="{{ asset('frontend2') }}/logo.png" alt="#"
										style="height: 80px !important;"></a>
							</div>
							<div class="footer_menu_item">
								<a href="{{ url('/') }}">Home</a>
								<a href="about.html">About</a>
								<a href="product_list.html">Products</a>
								<a href="blog.html">Blog</a>
								<a href="contact.html">Contact</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="social_icon">
							<a href="#"><i class="fab fa-facebook-f"></i></a>
							<a href="#"><i class="fab fa-instagram"></i></a>
							<a href="#"><i class="fab fa-google-plus-g"></i></a>
							<a href="#"><i class="fab fa-linkedin-in"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	-->
		<div class="copyright_part" style="padding: 10px 0px;">
			<div class="container">
				<div class="row ">
					<div class="col-lg-12">
						<div class="copyright_text">
							<P>
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>
									document.write(new Date().getFullYear());
								</script> All rights reserved
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</P>
							<div class="copyright_link">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--::footer_part end::-->

	<!-- jquery plugins here-->
	<script src="{{ asset('frontend2') }}/js/jquery-1.12.1.min.js"></script>
	<!-- popper js -->
	<script src="{{ asset('frontend2') }}/js/popper.min.js"></script>
	<!-- bootstrap js -->
	<script src="{{ asset('frontend2') }}/js/bootstrap.min.js"></script>
	<!-- magnific popup js -->
	<script src="{{ asset('frontend2') }}/js/jquery.magnific-popup.js"></script>
	<!-- carousel js -->
	<script src="{{ asset('frontend2') }}/js/owl.carousel.min.js"></script>
	<!-- slick js -->
	<script src="{{ asset('frontend2') }}/js/slick.min.js"></script>
	<script src="{{ asset('frontend2') }}/js/jquery.counterup.min.js"></script>
	<script src="{{ asset('frontend2') }}/js/waypoints.min.js"></script>
	<script src="{{ asset('frontend2') }}/js/contact.js"></script>
	<script src="{{ asset('frontend2') }}/js/jquery.ajaxchimp.min.js"></script>
	<script src="{{ asset('frontend2') }}/js/jquery.form.js"></script>
	<script src="{{ asset('frontend2') }}/js/jquery.validate.min.js"></script>
	<script src="{{ asset('frontend2') }}/js/mail-script.js"></script>
	<!-- custom js -->

	<script src="{{ asset('backend') }}/datatables/datatables.min.js"></script>

	<script>
		$(document).ready(function () {
			var flash = "{{ Session::has('status') }}";
			if (flash) {
				var status = "{{ Session::get('status') }}";
				swal('success', status, 'success');
			}
		});
	</script>

	@yield('script')
</body>

</html>