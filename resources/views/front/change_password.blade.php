@extends('layouts.master-fr')

@section('content-login')
<div class="col-lg-6 col-md-6">
	<div class="login_part_text text-center">
			<div class="login_part_text_iner">
					<h2>Halo, {{ auth()->user()->name }} !</h2>
					<p>Form untuk mengubah Password</p>
			</div>
	</div>
</div>
<div class="col-lg-6 col-md-6">
	<div class="login_part_form">
			<div class="login_part_form_iner">
					<h3>Ubah Password</h3>
					<form method="POST" action="{{ route('updatePassword') }}" id="login-form">
						@csrf							
							<div class="col-md-12 form-group p_star">
								<input type="password" class="form-control" id="old_password" name="old_password" value="" placeholder="Masukan Password Lama" required autofocus>
								@if ($errors->has('old_password'))
									<span class="text-danger" role="alert">
											<small>{{ $errors->first('old_password') }}</small>
									</span>
								@endif
								@if (session()->has('old_password'))
									<span class="text-danger" role="alert">
											<small>{{ session('old_password') }}</small>
									</span>
								@endif
							</div>
							<div class="col-md-12 form-group p_star">
								<input type="password" class="form-control" id="password" name="password" value="" placeholder="Masukan Password Baru">
								@if ($errors->has('password'))
									<span class="text-danger" role="alert">
											<small>{{ $errors->first('password') }}</small>
									</span>
								@endif
							</div>
							<div class="col-md-12 form-group p_star">
								<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="" placeholder="Tulis Ulang Password Baru">
							</div>
							<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="btn_3">
										Ubah Password
									</button>
							</div>
					</form>
			</div>
	</div>
</div>
@endsection
