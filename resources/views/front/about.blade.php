@extends('layouts.master-fr')

@section('content')
<div class="row">
	<div class="col-12">
		<div class="hero__item set-bg" data-setbg="{{ asset('frontend/undraw_moments_0y20.png') }}">
			<div class="hero__text">
					<h2>Tentang Kami</h2>
			</div>
		</div>
	</div>
	<div class="col-12">
		<hr class="mt-2 mb-2">
		<p style="line-height: 35px !important;">
			Sejarah singkat perusahaan YOSHIDA didirikan pada tahun 1998, YOSHIDA adalah salah satu  perusahaan produksi dan penjualan baju anak yang beralamat di jl. Jend Sudirman, No.316, Ciroyom, Kec. Andir, Kota Bandung, Jawa Barat 40182 yang berada di atas Gedung bengkel Raja Ban DUNLOP di lt.2. <br>
			Perusahaan YOSHIDA pada saat ini dalam manajemen dan penerimaan order produksi pakaian yang belum tersistem komputerisasi untuk pencatatan laporan seperti laporan -laporan yang dibutuhkan.<br><br>
 
			<strong>Visi</strong><br>
			Untuk menjadikan perusahaan produksi pakaian yang berkembang dan professional serta memiliki kualitas standar produk nasional dan menciptakan lowongan pekerjan di indonesia.<br>

			<strong>Misi</strong><br>
			<li>Menciptakan dan menggali peluang pasar yang potensial.</li>
			<li>Memajukan produksi pakaian yang berkualitas dengan desain yang sesuai pelanggan seperti memenuhi kebutuhan sekolah, instansi-instansi pemerintah atau swasta dan masyarakat.</li>
			<li>Mendorong berkembangnya ekonomi dan sektor usaha kecil dan menengah.</li>
		</p>
	</div>
</div>
@endsection