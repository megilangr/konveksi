@extends('layouts.master-fr')

@section('css')
<style>
	.card_area .product_count .product_count_item {
		width: 30px !important;
		height: 30px !important;
		line-height: 30px !important;
	}

	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	/* Firefox */
	input[type=number] {
		-moz-appearance: textfield;
	}
</style>
@endsection

@section('content-detail')
<div class="col-md-4 col-lg-4">
	<img src="{{ url($product->image) }}" alt="#" class="img-fluid">
</div>
<div class="col-md-8 col-lg-8">
	<div class="single_product_text" style="margin: 10px !important;">
		<form action="{{ url('/add-to-cart') }}" method="post">
		@csrf
		<input type="hidden" name="product_id" value="{{ $product->id }}" style="display: none;" readonly>
		<h4>{{ $product->name }}</h4>
		<p>
			{{ $product->description }}
		</p>
		<p style="margin-top: 5px;">Rp. {{ number_format($product->price, 0, ',', '.') }}</p>
		<p style="margin-top: 5px;">Jumlah Pemesanan &ensp; : </p>
		<div class="card_area" style="margin-top: 5px !important;">
			<div class="product_count_area" style="justify-content: left !important;">
				<div class="product_count d-inline-block" style="margin: 0px !important;">
					<span class="product_count_item inumber-decrement" id="minus"> <i class="ti-minus"></i></span>
					<input class="product_count_item input-number" type="number" value="1" min="1" name="qty" id="qty">
					<span class="product_count_item number-increment" id="plus"> <i class="ti-plus"></i></span>
				</div>
			</div>
		</div>
		<button class="genric-btn primary-border" style="margin-top: 10px !important;">Masukan Keranjang</button>
		</form>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#minus').on('click', function() {
			var qty = $('#qty').val();
			if (qty !== '0' && qty !== '1') {
				qty = parseInt(qty) - 1;
			} else {
				qty = 1
			}
			$('#qty').val(qty);
		});
		
		$('#plus').on('click', function() {
			var qty = $('#qty').val();
			qty = parseInt(qty) + 1;
			$('#qty').val(qty);
		});
	});
</script>
@endsection