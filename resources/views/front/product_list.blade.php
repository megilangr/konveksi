@extends('layouts.master-fr')

@section('content')
<div class="col-lg-3">
	<div class="card border-secondary" style="border-radius: 5px !important;">
		<div class="card-header bg-secondary text-white" style="border-radius: 0px !important;">
			Katalog Produk
		</div>
		<div class="card-body" style="border-radius: 0px !important; padding-bottom: 0px;">
			<form action="{{ url('/list-product') }}" method="get">
				<h6>Kategori Produk &ensp; : </h6>
				@foreach ($category as $item)
					<div class="form-check" style="padding-left: 40px; margin-top: 10px;">
						<input class="form-check-input" type="checkbox" name="category[]" value="{{ $item->slug }}" id="category{{$loop->iteration}}">
						<label class="form-check-label" for="category{{$loop->iteration}}">
							{{ $item->name }}
						</label>
					</div>
				@endforeach
				<div class="form-group" style="margin-top: 10px;">
					<label for="">Range Harga : </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1" style="border-radius: 0px;">Rp. </span>
						</div>
						<input type="number" class="form-control" placeholder="20000" name="harga_awal" aria-label="harga-awal" aria-describedby="basic-addon1" style="border-radius: 0px;">
					</div>
					<div class="text-center" style="margin-bottom: 4px;">
						<small>Sampai</small>
					</div>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon2" style="border-radius: 0px;">Rp. </span>
						</div>
						<input type="number" class="form-control" placeholder="20000" name="harga_akhir" aria-label="harga-akhir" aria-describedby="basic-addon2" style="border-radius: 0px;">
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-secondary btn-block" style="border-radius: 5px;">
						Filter Produk
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="col-lg-9">
	<div class="card border-white" style="border-radius: 0px !important; ">
		<div class="card-header bg-white border-white text-center" style="border-radius: 0px !important;">
			<h3>List Produk</h3>
		</div>
		<div class="card-body" style="border-radius: 0px; padding: 40px;">
			<div class="row">
				@forelse ($products as $item)
				<div class="col-lg-4 col-md-4 col-sm-6 {{ str_replace(' ','', $item->category->name) }}">
					<div class="single_product_item text-center">
						<a href="{{ url('product/detail', $item->id) }}">
							<img src="{{ url($item->image) }}" alt="#" class="img-fluid" style="height: 200px !important;">
						</a>
						<h3> <a href="{{ url('product/detail', $item->id) }}">{{ $item->name }}</a> </h3>
						<p>Rp. {{ number_format($item->price, 0, ',', '.') }}</p>
					</div>
				</div>
				@empty
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="single_product_item text-center">
						<h3> Belum Ada Data Produk</h3>
					</div>
				</div>
				@endforelse
			</div>
		</div>
	</div>
</div>
@endsection