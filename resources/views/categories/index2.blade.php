@extends('layouts.master-back')

@section('content')
<div class="card">
	<div class="card-body">
		<div class="row align-items-center">
			<div class="col">
				<h4 class="header-title">
					<i class="ti-view-list-alt"></i> &ensp;
					Data Kategori
				</h4>
			</div>
			<div class="col text-right act-button">
				<a href="{{ url('/category/create') }}" class="btn btn-sm btn-primary">
					Tambah Data
				</a>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-12">
				<div class="table-responsive">
					<!-- Projects table -->
					<table class="table align-items-center table-flush" id="table1">
						<thead class="thead-light">
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Nama</th>
								<th scope="col">Slug</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							@foreach ($categories as $item)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $item->name }}</td>
								<td><a class="badge badge-info text-white"><b>{{ $item->slug }}</b></a></td>
								<td class="text-right">
									<div class="dropdown">
										<a class="btn btn-sm btn-default btn-icon-only" href="#" role="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="ti-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
											<a class="dropdown-item" href="{{ route('category.edit', ['id' => $item->id]) }}">Edit Data</a>
											<a class="dropdown-item" href="#"
												onclick="event.preventDefault(); document.getElementById('delete{{ $loop->iteration }}').submit();">Hapus
												Data</a>
										</div>

										<form action="{{ route('category.destroy', ['id' => $item->id]) }}" method="post"
											id="delete{{ $loop->iteration }}" onsubmit="">
											@csrf
											@method('DELETE')
										</form>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#table1').DataTable();
	});
</script>
@endsection