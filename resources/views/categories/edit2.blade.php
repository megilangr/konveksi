@extends('layouts.master-back')

@section('content')
<div class="card">
	<div class="card-body">
		<div class="row align-items-center">
			<div class="col">
				<h4 class="header-title">
					<i class="ti-pencil-alt text-warning"></i> &ensp;
					Edit Data Kategori
				</h4>
			</div>
			<div class="col text-right act-button">
				<a href="{{ url('/category') }}" class="btn btn-sm btn-danger">
					Kembali
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-lg-8">
				<form role="form" method="post" action="{{ route('category.update', ['id' => $category->id])  }}"
					enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="form-group">
						<label for="" class="form-control-label">Nama Kategori : </label>
						<input type="text" name="name" id="name" class="form-control" placeholder="Masukan Nama Kategori..."
							value="{{ $category->name }}" autofocus required>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-sm btn-success">
							Simpan Perubahan
						</button>
						<button type="reset" class="btn btn-sm btn-danger">
							Reset Input
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function () {
		$('#name').focus();
	});
</script>
@endsection