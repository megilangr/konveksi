@extends('layouts.master-back')

@section('content')
<div class="card">
	<div class="card-body">
		<div class="row align-items-center">
			<div class="col">
				<h4 class="header-title">
					<i class="fa fa-check text-succces"></i> &ensp;
					Konfirmasi Pemesanan
				</h4>
			</div>
		</div>
		<hr>
		<div class="table-responsive">
			<!-- Projects table -->
			<table class="table align-items-center table-flush" id="table1">
				<thead class="thead-light">
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Pembeli</th>
						<th scope="col">Total</th>
						<th scope="col">Tanggal Pesan</th>
						<th scope="col">Status</th>
						<th scope="col" class="text-center">#</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($confirms as $item)
					<tr>
						
						<td>{{ $loop->iteration }}</td>
						<td>{{ $item->user->name }}</td>
						<td>Rp. {{ number_format($item->total_price,0) }}</td>
						<td>{{ $item->date }}</td>
						<td>
								@if($item->status == 'belum bayar')
										<button type="button" class="btn btn-sm btn-danger text-white">{{ ucwords($item->status) }}</button>
								@elseif($item->status == 'menunggu verifikasi')
										<button type="button" class="btn btn-sm btn-warning text-white">{{ ucwords($item->status) }}</button>
								@elseif($item->status == 'dibayar')
										<button type="button" class="btn btn-sm btn-success text-white ">{{ ucwords($item->status) }}</button>
								@else
										<button type="button" class="btn btn-sm btn-danger text-white">{{ ucwords($item->status) }}</button>
								@endif
						</td>
						<td class="text-center">
							<div class="btn-group">
								<a href="{{ url('/order/detail/'.$item->id) }}" target="_blank" class="btn btn-sm btn-info">Detail</a>
								<a href="{{ url('confirmAdmin/terima/'.$item->id) }}" class="btn btn-sm btn-success">Terima</a>
								<a href="{{ url('confirmAdmin/tolak/'.$item->id)}}" class="btn btn-sm btn-danger">Tolak</a>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>		
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#table1').DataTable();
	});
</script>
@endsection