@extends('layouts.master-back')	

@section('content')
<div class="card">
	<div class="card-body">
		<div class="row align-items-center">
			<div class="col">
				<h3 class="mb-0">
					<i class="ti-pencil-alt text-primary"></i> &ensp;
					Edit Data Pengguna
				</h3>
			</div>
			<div class="col text-right act-button">
				<a href="{{ url('/user') }}" class="btn btn-sm btn-danger">
					Kembali
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<hr>
			</div>
		</div>
		<div class="row mt-1">
			<div class="col-12">
				<form method="post" action="{{ route('user.update', $user->id)  }}" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label for="" class="form-control-label">Nama User : </label>
								<input type="text" name="name" id="name" class="form-control" placeholder="Masukan Nama Pengguna..." value="{{ $user->name }}" autofocus required>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="form-group">
								<label for="" class="form-control-label">E-Mail : </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
									</div>
									<input type="email" name="email" id="email" min="0" class="form-control" placeholder="Masukan E-Mail Pengguna..." value="{{ $user->email }}" required>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<div class="form-group">
								<label for="" class="form-control-label">Password : <i class="text-danger">*Kosongkan Bila Tak Ingin Merubah Password</i></label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-key"></i></span>
									</div>
									<input type="password" name="password" id="password" min="0" class="form-control" placeholder="Masukan Password Pengguna...">
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<div class="form-group">
								<label for="" class="form-control-label">Konfirmasi Password : </label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-lock"></i></span>
									</div>
									<input type="password" name="password_confirmation" id="password_confirmation" min="0" class="form-control" placeholder="Masukan Ulang Password Pengguna..." value="{{ $user->password_confirmation }}">
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4">
							<div class="form-group">
								<label for="" class="form-control-label">Hak Akses / Role : </label>
								<select name="role" id="role" class="form-control" required>
									<option value="">Pilih Role</option>
									<option value="admin" {{ $user->role == 'admin' ? 'selected':'' }}>Admin</option>
									<option value="operator" {{ $user->role == 'operator' ? 'selected':'' }}>Operator</option>
									<option value="customer" {{ $user->role == 'customer' ? 'selected':'' }}>Customer</option>
								</select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<button type="submit" class="btn btn-success">
									Tambah Data
								</button>
								<button type="reset" class="btn btn-danger">
									Reset Input
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#name').focus();
	});
</script>
@endsection