@extends('layouts.master-fr')

@section('content-login')
<div class="col-lg-6 col-md-6">
	<div class="login_part_form">
		<div class="login_part_form_iner">
			<h3>Daftar Sekarang <br> Untuk Melakukan Pemesanan</h3>
			<form method="POST" action="{{ route('register') }}">
				@csrf
				<div class="col-md-12 form-group p_star">
					<input type="text" class="form-control" id="name" name="name" value="" placeholder="Nama Pengguna">
					@if ($errors->has('name'))
					<span class="text-danger" role="alert">
						<small>{{ $errors->first('name') }}</small>
					</span>
					@endif
				</div>
				<div class="col-md-12 form-group p_star">
					<input type="text" class="form-control" id="email" name="email" value="" placeholder="E-Mail">
					@if ($errors->has('email'))
					<span class="text-danger" role="alert">
						<small>{{ $errors->first('email') }}</small>
					</span>
					@endif
				</div>
				<div class="col-md-12 form-group p_star">
					<input type="password" class="form-control" id="password" name="password" value="" placeholder="Password">
				</div>
				
				<div class="col-md-12 form-group p_star">
					<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="" placeholder="Tulis Ulang Password">
					@if ($errors->has('password'))
					<span class="text-danger" role="alert">
						<small>{{ $errors->first('password') }}</small>
					</span>
					@endif
				</div>
				<div class="col-md-12 form-group">
					<button type="submit" value="submit" class="btn_3">
						Daftar
					</button>
					<a class="lost_pass" href="{{ url('/login') }}">Sudah Punya Akun ?</a>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="col-lg-6 col-md-6">
	<div class="login_part_text text-center">
		<div class="login_part_text_iner">
			<h2>Sudah Punya Akun ??</h2>
			<a href="#" class="btn_3">Login</a>
		</div>
	</div>
</div>
@endsection