@extends('layouts.master-fr')

@section('content-login')
<div class="col-lg-6 col-md-6">
	<div class="login_part_text text-center">
			<div class="login_part_text_iner">
					<h2>Belum Punya Akun ??</h2>
					<p>Daftar Sekarang !!</p>
					<a href="{{ url('/register') }}" class="btn_3">Daftar</a>
			</div>
	</div>
</div>
<div class="col-lg-6 col-md-6">
	<div class="login_part_form">
			<div class="login_part_form_iner">
					<h3>Selamat Datang <br> Login Dulu Baru Pesan</h3>
					<form method="POST" action="{{ route('login') }}" id="login-form">
						@csrf
							<div class="col-md-12 form-group p_star">
									<input type="text" class="form-control" id="email" name="email" value="" placeholder="E-Mail">
									@if ($errors->has('email'))
										<span class="text-danger" role="alert">
												<small>{{ $errors->first('email') }}</small>
										</span>
									@endif
							</div>
							<div class="col-md-12 form-group p_star">
									<input type="password" class="form-control" id="password" name="password" value="" placeholder="Password">
							</div>
							<div class="col-md-12 form-group">
									<button type="submit" value="submit" class="btn_3">
											Masuk
									</button>
									<a class="lost_pass" href="#">Lupa Password ?</a>
							</div>
					</form>
			</div>
	</div>
</div>
@endsection
