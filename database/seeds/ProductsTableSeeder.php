<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$product = Product::firstOrCreate([
				'name' => 'Kemeja',
				'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				'price' => 85500,
				'weight' => 500,
				'image' => '/upload/products/kemeja.png',
				'status' => 'publish',
				'category_id' => 1
			]);
			$product = Product::firstOrCreate([
				'name' => 'Jeans',
				'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				'price' => 98000,
				'weight' => 500,
				'image' => '/upload/products/jeans.jpg',
				'status' => 'publish',
				'category_id' => 2
			]);
			$product = Product::firstOrCreate([
				'name' => 'Jaket Hoodie',
				'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				'price' => 112000,
				'weight' => 500,
				'image' => '/upload/products/jaket.jpg',
				'status' => 'publish',
				'category_id' => 3
			]);
    }
}