<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::firstOrCreate([
					'name' => 'Baju',
					'slug' => 'baju',
        ]);
        $category = Category::firstOrCreate([
					'name' => 'Celana',
					'slug' => 'celana',
        ]);
        $category = Category::firstOrCreate([
					'name' => 'Jaket',
					'slug' => 'jaket',
        ]);
    }
}
