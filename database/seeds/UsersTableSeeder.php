<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::firstOrCreate([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('admin'),
            'role' => 'admin',
				]);

        $user = User::firstOrCreate([
						'name' => 'operator',
						'email' => 'operator@mail.com',
						'password' => bcrypt('operator'),
						'role' => 'operator',
				]);

				$user = User::firstOrCreate([
            'name' => 'dummy',
            'email' => 'dummy@mail.com',
            'password' => bcrypt('dummy'),
            'role' => 'customer',
        ]);
    }
}
