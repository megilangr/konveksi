<?php

namespace App\Http\Controllers;

use App\Confirm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Order_Product;
use Illuminate\Support\Facades\Session;

class PaymentConfirmController extends Controller
{
		public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $title = 'List Konfirmasi Pembayaran Customer';
				$confirms = Order::where('status', 'verifikasi pembayaran')->orderBy('id','desc')->get();
        return view('payed.index', compact('confirms', 'title'));
    }

    public function terima($order_id)
    {
        $order = Order::where('id', $order_id)->first();
        $order->status = 'dibayar';
				$order->save();
				
				$confirm = Confirm::where('order_id', $order_id)->first();
				$confirm->status_order = 'dibayar';
				$confirm->save();

        $order_product = Order_Product::findOrFail($order_id);

        Session::flash('status','Berhasil di konfirmasi');
        return redirect()->route('confirmAdmin');
    }

    public function tolak($order_id)
    {
        $order = Order::where('id', $order_id)->first();
        $order->status = 'ditolak';
        $order->save();

				$confirm = Confirm::where('order_id',$order_id)->first();
				$confirm->update([
					'status_order' => 'ditolak'
				]);

        Session::flash('status','Berhasil di konfirmasi dengan status di tolak');
        return redirect()->route('confirmAdmin');
    }

//    public function detail($order_id)
//    {
//        $hasilArray = array('product'=>array());
//        $order = Order::where('id',$order_id)->first();
//        $hasilArray['receiver'] = $order->receiver;
//        $hasilArray['address'] = $order->address;
//
//        $products = Order_Product::where('order_id',$order_id)->get();
//
//        foreach ($products as $key => $product){
//            $productArray = array();
//            $productArray['name']   = $product->product['name'];
//            $productArray['qty']    = $product->qty;
//            $productArray['subtotal'] = number_format($product->subtotal,0);
//
//            array_push($hasilArray['product'], $productArray);
//        }
//
//        $hasil = $hasilArray;
//        return view('payed.detai', compact('hasil'));
//    }

    public function detail($id)
    {
			
			$order = Order::where('id', $id)->first();
			$details = Order_Product::where('order_id', $id)->get();
			return view('payed.detail', compact('details', 'order'));
    }
}
